all: ratrains

ratrains: main.o 
	g++ -Wall -g main.o -o ratrains -lpcap -std=c++11

main.o: main.cpp
	g++ -Wall -g -c main.cpp  -lpcap -std=c++11

clean:
	rm *o ratrains
