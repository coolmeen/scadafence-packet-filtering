/* 
 * File:   main.h
 * Author: ilya
 *
 * Created on November 9, 2015, 4:14 PM
 */
#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unordered_map>
#include <string>
#include <tr1/unordered_map>
#include <boost/functional/hash.hpp>

#ifndef MAIN_H
#define	MAIN_H


/* ethernet headers are always exactly 14 bytes [1] */
#define SIZE_ETHERNET 14

/* Ethernet addresses are 6 bytes */
#define ETHER_ADDR_LEN	6

/* Ethernet header */
/* IP header */
struct sniff_ip {
        u_char  ip_vhl;                 /* header length */
        u_char  ip_tos;                 /* type of service */
        u_short ip_len;                 /* total length */
        u_short ip_id;                  /* identification */
        u_short ip_off;                 /* fragment offset field */
        u_char  ip_ttl;                 /* time to live */
        u_char  ip_p;                   /* protocol */
        u_short ip_sum;                 /* checksum */
        in_addr_t ip_src,ip_dst;  /* source and dest address */
};
#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)

/* TCP header */
typedef u_int tcp_seq;

struct sniff_tcp {
        u_short th_sport;               /* source port */
        u_short th_dport;               /* destination port */
        uint32_t th_seq;                 /* sequence number */
        uint32_t th_ack;                 /* acknowledgement number */
        u_char  th_offx2;               /* data offset, rsvd */
    #define TH_OFF(th)      (((th)->th_offx2 & 0xf0) >> 4)
        u_char  th_flags;
        #define TH_FIN  0x01
        #define TH_SYN  0x02
        #define TH_RST  0x04
        #define TH_PUSH 0x08
        #define TH_ACK  0x10
        #define TH_URG  0x20
        #define TH_ECE  0x40
        #define TH_CWR  0x80
        #define TH_FLAGS        (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
        u_short th_win;                 /* window */
        u_short th_sum;                 /* checksum */
        u_short th_urp;                 /* urgent pointer */
};
#endif	/* MAIN_H */

struct Packet_map_Key
{
    u_short src_port; //source port
    u_short dest_port;//destination port
    in_addr_t         src_ip;//source ip
    in_addr_t         dest_ip;//destination ip

    Packet_map_Key(u_short src_port, u_short dest_port,in_addr_t src_ip, in_addr_t dest_ip) 
    {
            this->src_port = src_port;
            this-> dest_port = dest_port;
            this-> src_ip = src_ip;
            this-> dest_ip=dest_ip;
    }
 
  bool operator==(const Packet_map_Key &other) const
  { return (src_port == other.src_port
            && dest_port == other.dest_port
            && src_ip == other.src_ip
            && dest_ip==other.dest_ip );
  }
};

struct KeyHasher
{
  std::size_t operator()(const Packet_map_Key& k) const
  {
      using boost::hash_value;
      using boost::hash_combine;

      // Start with a hash value of 0    .
      std::size_t seed = 0;

      // Modify 'seed' member of 'Packet_map_Value' after the other:
      hash_combine(seed,hash_value(k.src_ip));
      hash_combine(seed,hash_value(k.src_port));
      hash_combine(seed,hash_value(k.dest_port));
      hash_combine(seed,hash_value(k.dest_ip));
      // Return the result.
      return seed;
  }
};

struct Packet_map_Value
{
    /*store initial absolute sequence number in order to calculate each iteration 
     * the relative sequence number. 
     */
    uint32_t absolute_sequence;
    uint32_t relative_sequence;
    
    timeval  ts; 
    uint32_t payload_size;
    
    Packet_map_Value()
    {
    
    }
    void Packet_map_Value_init( uint32_t absolute_sequence,uint32_t relative_sequence ,timeval  ts,  uint32_t payload_size) 
    {
            this->relative_sequence = relative_sequence;
             this->absolute_sequence = absolute_sequence;
             this->ts = ts;
             this->payload_size=payload_size;
    }
};