#include "main.h"


uint32_t retransmissions = 0;
uint32_t packetCount = 0;


//hash map for  <ip_src,ip_dst,source_port,dst_port> -> lastest<relative_sequence,absolute_sequence,time_stamp>;
std::unordered_map<Packet_map_Key, Packet_map_Value, KeyHasher> sequence_hash_map;

using namespace std;

/* default snap length (maximum bytes per packet to capture) */

double get_time() {
    struct timeval t;
    struct timezone tzp;
    gettimeofday(&t, &tzp);
    return t.tv_sec + t.tv_usec * 1e-6;
}

   
/*in order to find RETRANSMISSIONS we need to filter
    * packets that looks like RETRANSMISSIONS but actually has some another properties
 check if this packets <ip_src,ip_dst,source_port,dst_port> sequence number is:
 * 1. starts one byte prior to what should be the next sequence number and contains 0 or 1 bytes of data and -> KEEP A LIVE 
 * 2.doesn't look like what it should be and If the segment came 3ms since the segment with the highest seen sequence number 
 *    then it is an OUT-OF-ORDER 
 *    //FIRST NEED TO FILTER FAST RETRANSMISSIONS (not included in this patch)
 * 3.the same or less of the last highest transmitted sequence -> TCP RETRANSMISSIONS
 * 
 */

void got_packet(struct pcap_pkthdr *header, const u_char *packet) {

    /*
     this big hash map function that resides in the data is dangerous but much easy manageable
     in real life heap allocation would be better choice. 
     also using map is highly memory consuming.
     */
    const struct sniff_ip *ip; /* The IP header */
    const struct sniff_tcp *tcp; /* The TCP header */
    int size_ip;
    int size_tcp;
    uint32_t payload_size;
    packetCount++;
    /* define/compute ip header offset */
    ip = (struct sniff_ip*) (packet + SIZE_ETHERNET);
    size_ip = IP_HL(ip)*4;
    //check that the ip header is correct
    if (size_ip < 20) {
        return;
    }

    //make sure we are dealing with tcp packet
    if (ip->ip_p != IPPROTO_TCP) {
        return;
    }
    /*define/compute TCP header offset */
    tcp = (struct sniff_tcp*) (packet + SIZE_ETHERNET + size_ip);
    size_tcp = TH_OFF(tcp)*4;
    if (size_tcp < 20) {
        return;
    }

    payload_size = ntohs(ip->ip_len) - (size_ip + size_tcp);
    /*   check packet for retransmissions:
         may occur only if current payload_size is greater than zero or 
         if TH_SYN/TH_FIN is set
     */
    struct Packet_map_Key map_key(tcp->th_sport, tcp->th_dport, ip->ip_src, ip->ip_dst);
        //retrieves a value from the global hash_map
        struct Packet_map_Value *sequence_map_value = &sequence_hash_map[map_key];

    uint32_t tcp_seq = ntohl(tcp->th_seq);
        uint32_t tcp_reletive_seq = tcp_seq - sequence_map_value->absolute_sequence;
    if (payload_size > 0 || (tcp->th_flags & TH_FIN || tcp->th_flags & TH_SYN || tcp->th_flags & TH_RST)) 
    {
        
        //fist packet exchange
        if (sequence_map_value->absolute_sequence == 0) {
            sequence_map_value->Packet_map_Value_init(ntohl(tcp->th_seq), 0, header->ts, payload_size); //initilize 
            if (tcp->th_flags & TH_FIN || tcp->th_flags & TH_SYN)
                sequence_map_value->payload_size++;
            return;
        }

        //out of segment
         if( tcp_reletive_seq > sequence_map_value->relative_sequence + sequence_map_value->payload_size && (tcp->th_flags & TH_RST)==0)
             goto finilize;
        

        
        /* KEEP-ALIVE
         * contains 0 or 1 bytes of data and starts one byte prior
         * to what should be the next sequence number(sequence_map_value->payload_size+sequence_map_value->relative_sequence).
         * SYN/FIN/RST segments are never keep-alive-s
         */
        
        if ((sequence_map_value->payload_size + sequence_map_value->relative_sequence) == (tcp_reletive_seq + 1)
                && (payload_size == 1 || payload_size == 0) && !(tcp->th_flags & (TH_FIN | TH_SYN | TH_RST)))
            goto finilize;
         
        /* OUT-OF-ORDER  
         *If the segment came 3ms since the segment with the highest
         * seen sequence number and then it is an OUT-OF-ORDER segment and it doesn't look like a fast retransmission.
         *
         */
         
        double t = (header->ts.tv_sec - sequence_map_value->ts.tv_sec)*1000000000;
        t = t + header->ts.tv_usec - sequence_map_value->ts.tv_usec;
        if (t < 30000 && (tcp_reletive_seq + payload_size) != (sequence_map_value->relative_sequence + sequence_map_value->payload_size))
            goto finilize;
        /* RETRANSMISSION
         * if the current sequence number is less the the highest seen sequence number
         * seen so far its a RETRANSMISSION
         */
  
        if (tcp_reletive_seq < (sequence_map_value->relative_sequence + sequence_map_value->payload_size)) {
            // printf("%u\n", packetCount);
            retransmissions++;
        }

    }
        //add the last biggest known sequence occurrence to the hash
        //if we suspect the next reletive sequence number is gonna be roundded to zero, we store a new absolute sequence
finilize:
        if (tcp_reletive_seq >= sequence_map_value->relative_sequence + sequence_map_value->payload_size  || (tcp_reletive_seq + payload_size) < tcp_reletive_seq) {
            sequence_map_value->Packet_map_Value_init(sequence_map_value->absolute_sequence, tcp_reletive_seq, header->ts, payload_size);
            if (tcp->th_flags & TH_FIN || tcp->th_flags & TH_SYN)
                sequence_map_value->payload_size++;
            //check if we are overflowing the max sequence number
            //if true initialize again.
            if ((tcp_reletive_seq + payload_size) < tcp_reletive_seq) {
                sequence_map_value->absolute_sequence = tcp_seq; //seq number is in big endian, convert it to little endian
                sequence_map_value->relative_sequence = 0;
            }
                
            return;
        }
    return;
}

/*
    runs through the pcap file separating processing of each packet and counting the retranssmissions
 */

int main(int argc, char* argv[]) {

    if (argc <= 1) {
        printf("usuage:> pcap_trans filename\n");
        exit(0);
    }

    char *filename = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle;

    handle = pcap_open_offline(filename, errbuf);
    if (handle == NULL) {
        printf("error opening file %s %s\n", errbuf, filename);
        exit(0);
    }
    struct pcap_pkthdr *header;
    const u_char *data;

    double begining_time = get_time(); // get stating time to calculate sum of all time


    while (pcap_next_ex(handle, &header, &data) >= 0) {

        got_packet(header, data);

    }
    double end_time = get_time();
    //printf("time lapse, start:%f end:%f diff:%f\n", begining_time, end_time, end_time - begining_time);
    float packet_Precenage = (float) retransmissions / (float) packetCount * 100;
    printf("retransmissions precenege :%u %u %0.2f\n", retransmissions, packetCount, packet_Precenage);
    printf("packets for sec:%u\n", (uint32_t) (1 / end_time - begining_time * packetCount));

    return 1;
}
